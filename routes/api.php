<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', '\App\Http\Controllers\ApiAuthRegisterController@register');
Route::post('/login','\App\Http\Controllers\LoginController@login')->name('login');
Route::post('/refresh','\App\Http\Controllers\LoginController@refresh')->name('refresh');

Route::apiResource('cours', Controllers\CoursController::class);
Route::apiResource('token', Controllers\TokenController::class)->only('index');

Route::middleware('auth:api')->group(function () {
    Route::post('/logout','\App\Http\Controllers\LoginController@logout')->name('logout');
    Route::get('/user','\App\Http\Controllers\LoginController@infos')->name('user_info');
    Route::apiResource('token', Controllers\TokenController::class)->only(['store']);
    Route::apiResource('cours', Controllers\CoursController::class)->only(['update', 'store', 'destroy']);
    Route::post('/onScan', '\App\Http\Controllers\TokenController@onScan')->name('token.onScan');
    Route::post('/cours/addProf', '\App\Http\Controllers\CoursController@associateToAProf')->name('Associate_a_professor_to_a_course');
});

