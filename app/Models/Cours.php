<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, false|string $uid)
 * @method static orderBy(string $string)
 */
class Cours extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'denomination', 'description'];

    public static function create(array $all) {
        try {
            $model = static::query()->create($all);
            $model->uid = md5(uniqid());
            $model->save();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function tokens() {
        return $this->hasMany(Token::class, 'cours_id', 'id');
    }

    public function users(){
        return $this->belongsToMany(User::class, 'link_users_cours', 'cours_id', 'user_id');
    }
}
