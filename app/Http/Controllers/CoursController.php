<?php

namespace App\Http\Controllers;

use App\Models\Cours;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class CoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $user = Auth::User();
        if ($user->type == "student") {
            return response()->json([
                'error' => 'Le user n\'est pas un professeur'
            ]);
        }

        return \App\Http\Resources\Cours::collection($user->cours);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (Cours::create($request->all())) {
            $name = $request->get('name');
            $course = Cours::where('name', $name)->latest('created_at')->first();
            return response()->json([
                'success' => 'le cours a été ajouté',
                'cour_id' => $course->uid
            ]);
        }

        return response()->json([
            'error' => 'le cours n\'a pas été enregistré'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Cours $course
     * @return \App\Http\Resources\Cours|JsonResponse
     */
    public function show(Request $request, Cours $course)
    {
        $uid = substr($request->path(), strrpos($request->path(), '/') + 1);
        $course = Cours::where('uid', $uid)->first();

        if ($course) {
            return new \App\Http\Resources\Cours($course);
        } else {
            return response()->json([
                'error' => 'Aucun cours avec cette id'
            ], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Cours $course
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $uid = substr($request->path(), strrpos($request->path(), '/') + 1);
        $course = Cours::where('uid', $uid)->first();

        if ($course->update($request->all())) {
            return response()->json([
                'success' => 'le cours a été modifier'
            ]);
        }

        return response()->json([
            'error' => 'le cours n\'a pas été modifier'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        $uid = substr($request->path(), strrpos($request->path(), '/') + 1);
        $course = Cours::where('uid', $uid)->first();

        if ($course->delete()) {
            return response()->json([
                'success' => 'le cours a été supprimé'
            ]);
        }

        return response()->json([
            'error' => 'le cours n\'a pas été supprimé'
        ]);
    }

    public function associateToAProf(Request $request) {
        $body = $request->all();
        $user = Auth::User();

        if (!$body["cours_id"]) {
            return response()->json([
                'error' => 'Le cours n\'est pas renseigné'
            ]);
        }

        $cours_uid = $body["cours_id"];
        $cours = Cours::where('uid', $cours_uid)->first();

        if ($user->type == "professor") {
            if ($user->cours->where("uid", $cours_uid)->first()) {
                return response()->json([
                    'error' => 'Ce cours est déjà associé'
                ]);
            }

            $cours->users()->attach($user->id);
        } else {
            return response()->json([
                'error' => 'L\'utilisateur n\'est pas un professeur'
            ]);
        }

        return response()->json([
            'success' => 'Le cours à été associé au professeur'
        ]);
    }
}
