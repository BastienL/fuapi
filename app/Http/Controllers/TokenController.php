<?php

namespace App\Http\Controllers;

use App\Models\Cours;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use function PHPUnit\Framework\isNull;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection|Response
     */
    public function index()
    {
        return \App\Http\Resources\Token::collection(Token::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $body = $request->all();
        $cours_id = $body['cours_id'];
        $current_date = Carbon::now();

        $last_token = Cours::where('uid', $cours_id)->first()->tokens->last();

        if ($last_token != null) {
            $dateInSeconde = strtotime($last_token->created_at);
            /** @var int $dateInSeconde */
            $difference = $current_date->addSecond(-$dateInSeconde);
            $converted = strtotime($difference);
            
            if ($converted < 3600) {
                return response()->json([
                    'success' => 'le token a été généré',
                    'Token' => $last_token->uid
                ]);
            }
        }

        $token = Token::create($body);

        if ($token != null) {
            return response()->json([
                'success' => 'le token a été généré',
                'Token' => $token->uid
            ]);
        }

        return response()->json([
            'error' => 'le token n\'a pas été généré'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Token $token
     * @return Response
     */
    public function show(Token $token)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Token $token
     * @return Response
     */
    public function update(Request $request, Token $token)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Token $token
     * @return Response
     */
    public function destroy(Token $token)
    {
        //
    }

    /**
     * Add a user to a courses after a scan.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function onScan(Request $request)
    {
        $body = $request->all();

        $user = Auth::user();

        if (isNull($user) && empty($user)) {
            return response()->json([
                'error' => 'User not found'
            ], 400);
        }

        if ($user->type !== "student") {
            return response()->json([
                'error' => "L'utilisateur n'est pas du bon type"
            ], 400);
        }

        $tokenUid = $body['token_id'];

        if ($tokenUid == '') {
            return response()->json([
                'error' => 'Token not found'
            ], 400);
        }

        $token = Token::where('uid', $tokenUid)->first();

        if (isNull($token) && empty($token)) {
            return response()->json([
                'error' => 'Token not found'
            ], 400);
        }

        $courses = $token->cours;

        return response()->json([
            'success' => 'register done',
            'Token' => $token->uid,
            'cours' => $courses->denomination,
            'student' => $user->name
        ]);

    }
}
