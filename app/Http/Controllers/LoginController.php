<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class LoginController extends Controller
{
    private $client;

    /**
     * LoginController constructor.
     *
     * Set up the client to get secret info in private
     */
    public function __construct()
    {
        $this->client = Client::find(1);
    }

    /**
     * The login from the api.
     *
     * @param Request $request
     * @return access_token and refresh_token
     */
    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $params = [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => request('email'),
            'password' => request('password'),
            'scope' => '*'
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        $dispatch = Route::dispatch($proxy);

        return $dispatch;
    }

    /**
     * The refresh token from the api.
     *
     * @param Request $request
     * @return access_token and refresh_token
     */
    public function refresh(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required'
        ]);

        $params = [
            'grant_type' => 'refresh_token',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => request('email'),
            'password' => request('password')
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }

    /**
     * The logout from the api.
     *
     * makes access_token and refresh_token unusable
     */
    public function logout(): JsonResponse
    {
        $accessToken = Auth::User()->token();

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        return response()->json(['logged out'], 204);
    }

    /**
     * Get public information from the current user.
     * @return JsonResponse
     */
    public function infos(): JsonResponse
    {
        $user = Auth::User();

        return Response()->json(new \App\Http\Resources\User($user), 200);
    }
}
